# Gotify

This role installs and configures **Gotify server** and **Gotify CLI client**.

## Some manual configuration required

After running this role, login as `admin` in the web GUI and create
your other user accounts, as well as their apps.

## Pushing messages

You can push via `curl`, `HTTPie` or any other http-client.
You can also use the [Gotify CLI client](https://github.com/gotify/cli).

To push, you **must first create an application**. Easily done via the webUI.


## Refs

+ https://github.com/gotify/server
+ https://github.com/gotify/cli
+ https://gotify.net/docs/pushmsg
+ https://www.linux-magazine.com/Issues/2020/230/Gotify
+ https://gotify.net/docs/apache
+ https://blog.wains.be/2019/2019-03-16-gotify-self-hosted-push-notification/
+ https://hub.docker.com/r/gotify/server
+ https://gotify.net/docs/install
